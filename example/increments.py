"""
Example script for stepwise incrementing output
voltage on a Manson SDP or HCS power suply.
"""

from pyManson import manson
import time

# Serial port
port = "/dev/ttyUSB0"
# Mode
mode = 232
# RS485 Channel
channel = 255

msg = f"Connecting to {port} using RS{mode}"
if mode == 485:
    msg += f"@CH{channel}"
print(msg)

ps = manson(port, mode=mode, channel=channel)
ps.init_serial()
ps.begin_session()
ps.output_on()

ps.set_volts(0)
ps.begin_session()
ps.output_on()

# Minimum voltage
Vmin = 1  # [V]
# Maximum voltage
Vmax = 10  # [V]
# number of steps
N = 10
# Time between each step
dt = 2

# Perform steps
for n in range(0, N + 1):
    ps.set_volts(Vmin + n * (Vmax - Vmin) / float(N))
    time.sleep(dt / 2)
    U = ps.get_volts()
    I = ps.get_amps()
    print(f"U:\t{U:0.2f}\t I:\t{I:0.2f}")
    time.sleep(dt)
# Stop gracefully
ps.output_off()
ps.end_session()
