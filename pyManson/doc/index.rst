.. SMGAir documentation master file, created by
   sphinx-quickstart on Mon Jun 18 10:59:09 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

==================================================
 Manson power supply - serial communication 
==================================================

* Establish serial connection to supported Manson power supply 
* Read current device settings
* Control DC output
* Control device settings

Supported power supplies:
~~~~~~~~~~~~~~~~~~~~~
* Manson SDP-2210 
* Manson SDP-2405
* Manson SDP-2603

Tested with:
~~~~~~~~~~~~~~~~~~~~~
* Manson SDP-2405

Required packages:
~~~~~~~~~~~~~~~~~~~~~
* pyserial
* numpy
* sphinx
* sphinx_rtd_theme

Installation:
~~~~~~~~~~~~~~~~~~~~~


Example:
~~~~~~~~~~~~~~~~~~~~~




Manson SDP2405
-----------------
.. automodule:: mansonSDP2405

    .. autoclass:: mansonSDP2405
       :members:

