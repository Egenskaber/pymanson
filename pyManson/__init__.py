# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

__version__ = '1.2.0'
from .mansonClass import manson
